# README #

LaTazza is a program for managing a hot-drink vending machine. It supports two kinds of clients, visitors or employees, and allows the secretary to: manage sale and supply of small bags of beverages (e.g., coffee or tea), manage credits and debts of employees, and check inventory and cash account.

**Note that:** LaTazza handles one connection at a time to its database. If there are multiple connections (e.g., you have run a second instance of LaTazza without closing the first one), LaTazza raises a JdbcSQLException exception with the following error message: "Database may be already open: Locked by another process". If you are in this situation, close any instance of LaTazza and then run a new instance.

## Fixing issues ##

Create a commit (or more commits, if needed) to fix each issue. Remember to mention, in the commit message, the issue you are working on -- each issue has an ID.